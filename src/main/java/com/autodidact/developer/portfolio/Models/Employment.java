package com.autodidact.developer.portfolio.Models;

/*Created by smahalingam on 6/7/20.
 */
public class Employment {
    private String companyName;
    private String position;
    private String location;
    private String years;
    private String[] employmentResponsibilities;

    public Employment(){

    }
    public Employment(String companyName, String position, String location, String years,String[] employmentResponsibilities) {
        this.companyName = companyName;
        this.position = position;
        this.location = location;
        this.years = years;
        this.employmentResponsibilities=employmentResponsibilities;
    }

    public String[] getEmploymentResponsibilities() {
        return employmentResponsibilities;
    }

    public void setEmploymentResponsibilities(String[] employmentResponsibilities) {
        this.employmentResponsibilities = employmentResponsibilities;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getYears() {
        return years;
    }

    public void setYears(String years) {
        this.years = years;
    }
}
