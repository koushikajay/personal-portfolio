package com.autodidact.developer.portfolio.Models;
/* Created by smahalingam on 6/28/20.*/

public class ContactInfo {
    private String Name;
    private String emailAddress;
    private String message;

    public ContactInfo(String Name,String emailAddress,String message){
        this.Name=Name;
        this.emailAddress=emailAddress;
        this.message=message;

    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
