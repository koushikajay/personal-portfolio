package com.autodidact.developer.portfolio.Models;

/* Created by smahalingam on 6/7/20.*/
public class Education {

    private String degree;
    private String university;
    private String location;
    private Double gpa;
    private String graduationYear;

    public Education(){

    }
    public Education(String degree,String university,String location,Double gpa,String graduationYear){
        this.degree=degree;
        this.university=university;
        this.location=location;
        this.gpa=gpa;
        this.graduationYear=graduationYear;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setGpa(Double gpa) {
        this.gpa = gpa;
    }

    public void setGraduationYear(String graduationYear) {
        this.graduationYear = graduationYear;
    }

    public String getDegree() {
        return degree;
    }

    public String getUniversity() {
        return university;
    }

    public String getLocation() {
        return location;
    }

    public Double getGpa() {
        return gpa;
    }

    public String getGraduationYear() {
        return graduationYear;
    }
}
