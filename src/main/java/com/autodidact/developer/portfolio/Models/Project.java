package com.autodidact.developer.portfolio.Models;


import java.util.List;

/**
 * Created by smahalingam on 5/19/20.
 */
public class Project {

    private String title;
    private String description;
    private List<String> techStack;
    private String gitUrl;

    public String getGitUrl() {
        return gitUrl;
    }

    public void setGitUrl(String gitUrl) {
        this.gitUrl = gitUrl;
    }

    public List<String> getTechStack() {
        return techStack;
    }

    public void setTechStack(List<String> techStack) {
        this.techStack = techStack;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public Project(){

    }
    public Project(String title, String description,List<String> techStack){
        this.title=title;
        this.description=description;
        this.techStack=techStack;
    }
    public String getTitle() {
        return title;
    }

    public String getDescription() {

        return description;
    }

}
