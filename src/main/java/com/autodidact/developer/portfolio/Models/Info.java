package com.autodidact.developer.portfolio.Models;

import java.util.ArrayList;
import java.util.Map;

/*Created by smahalingam on 6/7/20. */
public class Info {
    private String name;
    private ArrayList<Education> educations;
    private ArrayList<Employment> employments;
    private ArrayList<Project> projects;
    private Map<String,String> skills;
    public  Info(){

    }
    public Info(String name, ArrayList<Education> educations, ArrayList<Employment> employments, ArrayList<Project> projects,Map<String,String> skills) {
        this.name = name;
        this.educations = educations;
        this.employments = employments;
        this.projects = projects;
        this.skills=skills;
    }

    public Map<String, String> getSkills() {
        return skills;
    }

    public void setSkills(Map<String, String> skills) {
        this.skills = skills;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Education> getEducations() {
        return educations;
    }

    public void setEducations(ArrayList<Education> educations) {
        this.educations = educations;
    }

    public ArrayList<Employment> getEmployments() {
        return employments;
    }

    public void setEmployments(ArrayList<Employment> employments) {
        this.employments = employments;
    }

    public ArrayList<Project> getProjects() {
        return projects;
    }

    public void setProjects(ArrayList<Project> projects) {
        this.projects = projects;
    }
}
