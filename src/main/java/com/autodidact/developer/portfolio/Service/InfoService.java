package com.autodidact.developer.portfolio.Service;
/* Created by smahalingam on 6/8/20.*/

import com.autodidact.developer.portfolio.Models.Education;
import com.autodidact.developer.portfolio.Models.Employment;
import com.autodidact.developer.portfolio.Models.Info;
import com.autodidact.developer.portfolio.Models.Project;


import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface InfoService {
    public abstract Info info() throws IOException;
    public abstract List<Education> getEducations() throws IOException;
    public abstract List<Employment> getEmployments() throws IOException;
    public abstract List<Project> getProjects() throws IOException;
    public abstract Map<String,String> getSkills() throws IOException;

}
