package com.autodidact.developer.portfolio.Service;
/* Created by smahalingam on 6/8/20.*/

import com.autodidact.developer.portfolio.Models.Education;
import com.autodidact.developer.portfolio.Models.Employment;
import com.autodidact.developer.portfolio.Models.Info;
import com.autodidact.developer.portfolio.Models.Project;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.Reader;
import java.nio.channels.Channels;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
public class InfoServiceImpl implements InfoService {
    @Value("classpath:info.json")
    public Resource resourceInfo;

    @Override
    @Bean
    public Info info() throws IOException{
        Reader reader = Channels.newReader(resourceInfo.readableChannel(), StandardCharsets.UTF_8.name());
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(reader,Info.class);
    }

    @Override
    public List<Project> getProjects() throws IOException {
        return info().getProjects();
    }


    @Override
    public List<Education> getEducations() throws IOException {
        return info().getEducations();
    }

    @Override
    public List<Employment> getEmployments() throws IOException{
        return info().getEmployments();
    }

    @Override
    public Map<String, String> getSkills() throws IOException {
        return info().getSkills();
    }
}
