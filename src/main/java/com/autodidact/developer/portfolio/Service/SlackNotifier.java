package com.autodidact.developer.portfolio.Service;
/* Created by smahalingam on 6/28/20.*/

import com.hubspot.slack.client.SlackClient;
import com.hubspot.slack.client.methods.params.chat.ChatPostMessageParams;

public class SlackNotifier {
    private SlackClient slackClient;
    private String channel;

    public SlackNotifier(SlackClient slackClient, String channel) {
        this.slackClient = slackClient;
        this.channel = channel;
    }

    public void postMessage(String message) {

        slackClient.postMessage(
                ChatPostMessageParams.builder()
                        .setText(message)
                        .setChannelId(channel)
                        .build()
        ).join().unwrapOrElseThrow();
    }
}
