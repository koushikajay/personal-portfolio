package com.autodidact.developer.portfolio.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by smahalingam on 5/1/20.
 */
@Controller
public class Home {
    @RequestMapping({ "/", "/home" })
    public String home(){
        return "home";
    }
}
