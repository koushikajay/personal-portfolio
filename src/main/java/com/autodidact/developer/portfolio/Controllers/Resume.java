package com.autodidact.developer.portfolio.Controllers;

import com.autodidact.developer.portfolio.Service.InfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;

/**
 * Created by smahalingam on 5/1/20.
 */
@Controller
public class Resume {
    @Autowired
    InfoService infoService;

    @RequestMapping("/resume")
    public String resume(Model model) throws IOException{
        model.addAttribute("employments",infoService.getEmployments());
        model.addAttribute("educations",infoService.getEducations());
        model.addAttribute("skills",infoService.getSkills());
        return "resume";
    }
}
