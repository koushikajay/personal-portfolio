package com.autodidact.developer.portfolio.Controllers;

import com.autodidact.developer.portfolio.Models.Project;
import com.autodidact.developer.portfolio.Service.InfoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;
import java.io.Reader;
import java.nio.channels.Channels;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;


/**
 * Created by smahalingam on 5/1/20.
 */
@Controller
public class Projects {
    @Autowired
    InfoService infoService;

    @RequestMapping("/projects")
    public String projects(Model model) throws IOException {
        model.addAttribute("projects",infoService.getProjects());
        return "projects";
    }


}
