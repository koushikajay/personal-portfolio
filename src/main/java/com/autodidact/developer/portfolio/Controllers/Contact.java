package com.autodidact.developer.portfolio.Controllers;

import com.autodidact.developer.portfolio.Models.ContactInfo;
import com.autodidact.developer.portfolio.Service.SlackNotifier;
import com.hubspot.slack.client.SlackClient;
import com.hubspot.slack.client.SlackClientFactory;
import com.hubspot.slack.client.SlackClientRuntimeConfig;
import org.omg.CORBA.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
/**
 * Created by smahalingam on 5/1/20.
 */
@Controller
public class Contact {
//    @Value("${slack.api.token}")
    private String slackAPIToken=System.getProperty("slack.api.token");


    SlackClientRuntimeConfig runtimeConfig = SlackClientRuntimeConfig.builder()
            .setTokenSupplier(() -> slackAPIToken)
            .build();

    SlackClient slackClient = SlackClientFactory.defaultFactory().build(runtimeConfig);

    SlackNotifier slackNotifier = new SlackNotifier(slackClient, "general");
    @RequestMapping("/contact")
    public String contact(ContactInfo contactInfo){
        return "contact";
    }

    @PostMapping("/contact")
    public String sendMessage(ContactInfo contactInfo){
        String name= String.format("Sender Name: %s",contactInfo.getName());
        String emailAddress= String.format("Sender Email Address: %s",contactInfo.getEmailAddress());
        String comments=String.format("Message: %s",contactInfo.getMessage());
        String message = String.join("\n"
                , name
                , emailAddress
                , comments);

        slackNotifier.postMessage(message);
        return "redirect:/contact";
    }
}
